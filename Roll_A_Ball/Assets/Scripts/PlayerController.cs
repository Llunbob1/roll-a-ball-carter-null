﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;


public class PlayerController : MonoBehaviour
{



    public float speed = 2;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public GameObject Exit1;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;
    

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;


        SetCountText();
        winTextObject.SetActive(false);
        Exit1.SetActive(true);


    }




    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }


    void SetCountText()
    {

        countText.text = "Count: " + count.ToString();
        if(count >= 12)
        {
            winTextObject.SetActive(true);
        }
        if(count >= 11)
        {
            Exit1.SetActive(false);
        }
    }



    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);

    }


    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
            transform.Translate(Vector3.up * speed * Time.deltaTime);





    }


        private void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.CompareTag("PickUp"))

            {

                other.gameObject.SetActive(false);
                count = count + 1;

                SetCountText();

            }
  

    }



}
